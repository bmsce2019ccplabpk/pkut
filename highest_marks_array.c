#include<stdio.h>
void read(int a[5][3])
{
    for(int i=0;i<5;i++)
    {
        printf("enter the marks of student %d \n",i+1);
        for(int j=0;j<3;j++)
        {
            printf("subject %d :",j+1);
            scanf("%d",&a[i][j]);
        }
        printf("\n");
    }
}
void highest(int a[5][3],int *subj1,int *subj2,int *subj3)
{
    int i;
    for(i=0;i<5;i++)
    {
        if(*subj1<a[i][0])
        {
            *subj1=a[i][0];
        }
        if(*subj2<a[i][1])
        {
            *subj2=a[i][1];
        }
        if(*subj2<a[i][2])
        {
            *subj3=a[i][2];
        }
    }
}
void output(int h1,int h2,int h3)
{
    printf("highest marks in subject1 is %d \n",h1);
    printf("highest marks in subject2 is %d \n",h2);
    printf("highest marks in subject3 is %d \n",h3);
}
int main()
{
    int a[5][3],subj1=0,subj2=0,subj3=0;
    read(a);
    highest(a,&subj1,&subj2,&subj3);
    output(subj1,subj2,subj3);
    return 0;
}